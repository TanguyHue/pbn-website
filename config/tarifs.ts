type tarifsType = {
  title: string;
  description: string;
  prix: string;
};

export const tarifs: tarifsType[] = [
  {
    title: "Soirée",
    description: "Tarif pour accéder à la soirée PBN 2024. Accessible à tous.",
    prix: "20 €",
  },
  {
    title: "Repas uniquement",
    description: "Tarif pour les diplômés uniquement.",
    prix: "30 €",
  },
  {
    title: "Repas + Soirée",
    description: "Tarif pour les diplômés uniquement.",
    prix: "45 €",
  },
];
