export type SiteConfig = typeof siteConfig;

export const siteConfig = {
  name: "Polytech By Night 2024",
  description: "Site pour l'édition 2024 de Polytech By Night",
  navItems: [
    {
      label: "Le Gala",
      href: "/gala",
    },
    {
      label: "Infos pratiques",
      href: "/infos",
    },
    {
      label: "Qui sommes-nous ?",
      href: "/qui-sommes-nous",
    },
    {
      label: "Billetterie",
      href: "/billetterie",
    },
  ],
  footerItems: [
    {
      label: "Instagram",
      href: "https://www.instagram.com/galapolytechnantes/",
    },
    {
      label: "Facebook",
      href: "https://www.facebook.com/polytechbynight/",
    },
  ],
};

export const lienBilletterie =
  "https://www.helloasso.com/associations/polytech-by-night/evenements/gala-polytech-nantes-edition-23";
