export const sponsors = [
  {
    name: "BDE Polytech Nantes",
    src: "/assets/sponsors/bde.jpg",
    href: "https://www.instagram.com/bde.polytech.nantes/",
  },
  {
    name: "Université de Nantes",
    src: "/assets/sponsors/univ.png",
    href: "https://www.univ-nantes.fr/",
  },
  {
    name: "Polytech Nantes",
    src: "/assets/sponsors/polytech.png",
    href: "https://www.polytech.univ-nantes.fr/",
  },
  {
    name: "Mairie de Nantes",
    src: "/assets/sponsors/nantes.webp",
    href: "https://www.nantes.fr/",
  },
  {
    name: "3 Dom Night",
    src: "/assets/sponsors/3domnight.jpeg",
    href: "https://www.instagram.com/3domnight/",
  },
  {
    name: "U Tech",
    src: "/assets/sponsors/utech.png",
    href: "https://u-emploi.com/organisation-cooperative-u/u-tech",
  },
  {
    name: "Lyf",
    src: "/assets/sponsors/lyf.png",
    href: "https://www.lyf.eu/fr/",
  },
  {
    name: "Crédit Mutuel",
    src: "/assets/sponsors/creditMutuel.jpg",
    href: "https://www.creditmutuel.fr/home/",
  },
];
