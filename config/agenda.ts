export type agendaType = {
  hour: string;
  title: string;
  label: string;
};

export const agenda: agendaType[] = [
  {
    hour: "21h00",
    title: "Ouverture des portes",
    label: "Arrivez tôt pour profiter du décors dès le début ! 🥳",
  },
  {
    hour: "22h00",
    title: "Début des Festivités",
    label:
      "La soirée commence avec des performances en live et des surprises ! 🎶",
  },
  {
    hour: "03h15",
    title: "Clôture de la Soirée",
    label: "Fin des festivités et retour en toute sécurité. 🚗🚌",
  },
];
