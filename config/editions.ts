type editionType = {
  year: string;
  link: string;
  img: string;
};

export const editions: editionType[] = [
  {
    year: "2023",
    link: "https://pbn.univ-nantes.io/index.html",
    img: "/assets/edition_precedente/2023.png",
  },
  {
    year: "2022",
    link: "https://pbn.univ-nantes.io/2022/",
    img: "/assets/edition_precedente/2022.png",
  },
  {
    year: "2021",
    link: "https://pbn.univ-nantes.io/2021/",
    img: "/assets/edition_precedente/2021.png",
  },
];
