# PBN - Polytech By Night 🎆

Bienvenue sur **PBN (Polytech By Night)**, le site officiel du gala de **Polytech Nantes** 🎉. Ce projet est développé avec **Next.js**, **NextUI**, et stylé avec **Tailwind CSS** pour garantir performance, réactivité, et un design moderne.

🔗 **URL du site :** [www.polytech-by-night.fr](https://www.polytech-by-night.fr)

---

## 🚀 Technologies utilisées

- **Next.js** - Framework React pour le rendu côté serveur et côté client.
- **NextUI** - Librairie de composants UI pour un design moderne et accessible.
- **Tailwind CSS** - Un framework de styles utilitaire pour construire des interfaces rapides et réactives.

---

## 📂 Structure du projet

Voici un aperçu rapide de l'arborescence du projet :

```
PBN/
├── public/         # Assets publics (images, icônes, etc.)
├── components/        # Composants réutilisables
├── app/               # Pages principales du site
├── styles/            # Styles globaux et Tailwind config
├── tailwind.config.js # Configuration de Tailwind
├── next.config.js     # Configuration de Next.js
├── package.json       # Dépendances et scripts
└── README.md          # Ce fichier 😎
```

---

## 🛠️ Installation et démarrage du projet

### 1. Prérequis

Assurez-vous d'avoir les éléments suivants installés sur votre machine :

- **Node.js** (version 18.18 ou supérieure)
- **npm** ou **pnpm**

### 2. Cloner le dépôt

```bash
git clone https://gitlab.com/TanguyHue/pbn-website.git
```

### 3. Installer les dépendances

Utilisez npm ou pnpm pour installer les dépendances du projet :

```bash
npm install
# ou
pnpm i
```

### 4. Lancer le serveur de développement

Une fois les dépendances installées, démarrez le serveur de développement avec :

```bash
npm run dev
# ou
pnpm dev
```

Le site sera disponible à l'adresse suivante : [http://localhost:3000](http://localhost:3000)

### 5. Build pour la production

Pour compiler le site en vue d'une mise en production :

```bash
npm run build
# ou
pnpm build
```

Cela génère un dossier `.next` contenant le build optimisé.

---

## 📦 Déploiement

Le site est déployé sur un serveur en production. Pour déployer une nouvelle version, suivez ces étapes :

1. Assurez-vous d'avoir buildé le projet avec `npm run build` ou `pnpm build`.
2. Déployer le dossier `.next` sur votre serveur (ex : Vercel, Netlify ou un hébergement personnalisé).

---

## 🎯 Objectifs du projet

Ce projet a pour but de créer un site web attractif et fonctionnel pour l'événement **Polytech By Night**, permettant aux étudiants de Polytech Nantes de s'informer et de s'inscrire facilement à leur gala annuel.

---

## 🧑‍💻 Contribuer

Les contributions sont les bienvenues ! Si vous souhaitez ajouter des fonctionnalités ou corriger des bugs, n'hésitez pas à :

1. **Forker** ce dépôt.
2. **Créer une branche** (`git checkout -b ma-branche-feature`).
3. **Commiter** vos changements (`git commit -m 'Ajout d'une fonctionnalité incroyable'`).
4. **Pousser** votre branche (`git push origin ma-branche-feature`).
5. Ouvrir une **pull request**.

---

## 🎉 Remerciements

Merci à tous les contributeurs et à l'équipe du gala pour leur engagement à rendre cet événement inoubliable pour tous les participants.

---

## 📧 Contact

Pour toute question, suggestion ou problème, vous pouvez nous contacter via :

- **Email** : contact@polytech-by-night.fr
- **Site web** : [www.polytech-by-night.fr](https://www.polytech-by-night.fr)

---

Enjoy the party! 🎊🎉
--- 