"use client";

import {
  Navbar as NextUINavbar,
  NavbarContent,
  NavbarBrand,
  NavbarItem,
  NavbarMenuItem,
  NavbarMenu,
  NavbarMenuToggle,
} from "@nextui-org/navbar";
import { link as linkStyles } from "@nextui-org/theme";
import NextLink from "next/link";
import clsx from "clsx";
import { Image } from "@nextui-org/image";
import { usePathname } from "next/navigation";
import { useState } from "react";

import { siteConfig } from "@/config/site";

export const Navbar = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false);
  const pathname = usePathname();

  return (
    <NextUINavbar
      className="bg-beige pb-7"
      isMenuOpen={isMenuOpen}
      maxWidth="2xl"
      position="static"
      onMenuOpenChange={setIsMenuOpen}
    >
      <NavbarContent
        className="basis-1/2 sm:basis-full border-y-2 border-black mt-8"
        justify="start"
      >
        <div className="flex justify-between w-full mx-4 h-full items-center">
          <div>
            <NavbarBrand as="li" className="gap-3 max-w-fit">
              <NextLink
                className="flex justify-between items-center gap-1 transition duration-500 md:hover:text-gray-600"
                href="/"
                onClick={() => setIsMenuOpen(false)}
              >
                <Image
                  alt={siteConfig.name}
                  className="w-12 rounded-none"
                  src={"/favicon.ico"}
                  width={40}
                />
                <span className="ml-2 font-bold text-2xl">
                  Polytech By Night
                </span>
              </NextLink>
            </NavbarBrand>
          </div>
          <div className="hidden md:block">
            <ul className="gap-4 ml-2 flex justify-end items-center">
              {siteConfig.navItems.map((item) => (
                <NavbarItem key={item.href} isActive={pathname === item.href}>
                  <div
                    className={clsx("transition duration-500 rounded-xl", {
                      "hover:bg-beige-fonce hover:text-black ":
                        item.label === "Billetterie",
                      "hover:-translate-y-1": item.label !== "Billetterie",
                    })}
                  >
                    <NextLink
                      className={clsx(
                        linkStyles({ color: "foreground" }),
                        "data-[active=true]:text-primary data-[active=true]:font-bold",
                        {
                          "border-2 border-black border-spacing-4 rounded-xl p-2 text-xl":
                            item.label === "Billetterie",
                          "text-2xl": item.label !== "Billetterie",
                        },
                      )}
                      color="foreground"
                      href={item.href}
                    >
                      {item.label}
                    </NextLink>
                  </div>
                </NavbarItem>
              ))}
            </ul>
          </div>
          <div className="flex md:hidden">
            <NavbarMenuToggle
              aria-label={isMenuOpen ? "Close menu" : "Open menu"}
              className="p-2"
            />
            <NavbarMenu className="bg-beige">
              {siteConfig.navItems.map((item) => (
                <NavbarMenuItem
                  key={item.href}
                  className="mt-8 mx-5"
                  isActive={pathname === item.href}
                >
                  <NextLink
                    color="foreground"
                    href={item.href}
                    onClick={() => setIsMenuOpen(false)}
                  >
                    <div
                      className={clsx("w-full", {
                        "border-2 border-black border-spacing-4 rounded-xl p-2 text-xl text-center":
                          item.label === "Billetterie",
                      })}
                    >
                      {item.label}
                    </div>
                  </NextLink>
                </NavbarMenuItem>
              ))}
            </NavbarMenu>
          </div>
        </div>
      </NavbarContent>
    </NextUINavbar>
  );
};
