import { Button } from "@nextui-org/button";
import { Image } from "@nextui-org/image";

import { editions } from "@/config/editions";

export const EditionsPrecedentes = () => {
  return (
    <div className="flex flex-col md:flex-row items-center justify-center gap-4">
      {editions.map((edition) => (
        <div
          key={edition.year}
          className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1 w-full md:w-60"
        >
          <Button
            as="a"
            className="text-xl flex flex-col items-center justify-between w-full md:w-60 h-40 p-4 pt-8 shadow-xl"
            color="primary"
            href={edition.link}
            size="lg"
            target="_blank"
          >
            <Image
              className="w-32 object-cover rounded-none"
              src={edition.img}
            />
            <span className="pb-2 font-bold">Édition {edition.year}</span>
          </Button>
        </div>
      ))}
    </div>
  );
};
