import { tarifs } from "@/config/tarifs";

export const Tarifs = () => {
  return (
    <section className="py-12">
      <div className="container mx-auto px-8 text-center">
        <h2 className="text-4xl font-bold mb-6">🎟️ Tarifs des billets</h2>
        <div className="grid grid-cols-1 md:grid-cols-3 gap-8">
          {tarifs.map((tarif) => (
            <div
              key={tarif.title}
              className="bg-beige-fonce rounded-lg shadow-lg p-6 transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
            >
              <h3 className="text-2xl font-bold mb-4">{tarif.title}</h3>
              <p className="text-lg md:text-xl mb-4">{tarif.prix}</p>
              <p className="text-sm md:text-md text-gray-600">
                {tarif.description}
              </p>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};
