import { Button } from "@nextui-org/button";
import { useState } from "react";

import { lienBilletterie } from "@/config/site";

export const BilletterieAccess = () => {
  const [iframeOpen, setIframeOpen] = useState(false);

  if (true) {
    return (
      <div className="flex flex-col md:block">
        <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1 mb-8">
          <Button
            className="mb-4 md:mb-0 text-xl shadow-xl py-4 w-full md:w-60"
            color="primary"
            size="lg"
            onClick={() => setIframeOpen(!iframeOpen)}
          >
            <span className="font-bold">
              {iframeOpen ? "Fermer la réservation" : "Ouvrir la réservation"}
            </span>
          </Button>
        </div>
        {iframeOpen && (
          <div className="mb-8">
            <iframe
              allowFullScreen
              aria-hidden="false"
              className="w-full h-[750px] rounded-lg shadow-lg py-4"
              src={`${lienBilletterie}/widget`}
              title="Billetterie Polytech By Night"
            />
          </div>
        )}
        <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
          <Button
            as="a"
            className="mb-4 md:mb-0 text-xl shadow-xl py-4 w-full md:w-80"
            color="success"
            href={`${lienBilletterie}`}
            rel="noopener noreferrer"
            size="lg"
            target="_blank"
          >
            <span className="font-bold text-white">Réserver sur HelloAsso</span>
          </Button>
        </div>
      </div>
    );
  } else {
    return (
      <div>
        <h2 className="text-xl md:text-2xl mb-2">{`😢 Pas encore ouverte ! 🕐`}</h2>
        <p className="my-2 text-xl md:text-2xl">
          {`La billetterie ouvrira le 14 octobre 2024 à 00h00 !`}
        </p>
      </div>
    );
  }
};
