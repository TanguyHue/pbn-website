import { Button } from "@nextui-org/button";
import { Image } from "@nextui-org/image";

export const Contact = () => {
  return (
    <section className="py-12">
      <div className="container mx-auto px-8">
        <h2 className="text-4xl font-bold mb-6 text-center">📩 Contact</h2>
        <p className="text-lg md:text-xl mb-8 text-justify md:text-center">
          {`Vous avez des questions ou souhaitez en savoir plus sur Polytech
        By Night ? N'hésitez pas à nous contacter ! Nous serons ravis de
        vous répondre.`}
        </p>
        <div className="flex flex-col md:flex-row  justify-center gap-4">
          <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
            <Button
              as="a"
              className="mb-4 md:mb-0 text-xl w-full shadow-xl py-4"
              color="primary"
              href="mailto:contact@polytech-by-night.fr"
              size="lg"
              target="_blank"
            >
              <div className="flex items-center ">
                <Image
                  className="h-10"
                  src="/assets/reseaux-sociaux/mail.png"
                />
                <span className="ml-2 font-semibold">Envoyer un Email</span>
              </div>
            </Button>
          </div>
          <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
            <Button
              as="a"
              className="mb-4 md:mb-0 text-xl w-full shadow-xl py-4"
              color="primary"
              href="https://www.instagram.com/galapolytechnantes/"
              size="lg"
              target="_blank"
            >
              <div className="flex items-center ">
                <Image
                  className="h-10"
                  src="/assets/reseaux-sociaux/instagram.png"
                />
                <span className="ml-2 font-semibold">Instagram</span>
              </div>
            </Button>
          </div>
          <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
            <Button
              as="a"
              className="mb-4 md:mb-0 text-xl w-full shadow-xl"
              color="primary"
              href="https://www.facebook.com/polytechbynight/"
              size="lg"
              target="_blank"
            >
              <div className="flex items-center ">
                <Image
                  className="h-10"
                  src="/assets/reseaux-sociaux/facebook.png"
                />
                <span className="ml-2 font-semibold">Facebook</span>
              </div>
            </Button>
          </div>
        </div>
      </div>
    </section>
  );
};
