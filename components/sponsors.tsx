import Image from "next/image";
import NextLink from "next/link";

import { sponsors } from "@/config/listSponsors";

export const SponsorsSection = () => {
  return (
    <div className="flex justify-center">
      <div className="grid grid-cols-2 md:grid-cols-4 gap-8 justify-center">
        {sponsors.map((sponsor) => (
          <NextLink
            key={sponsor.name}
            className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
            href={sponsor.href}
            target="_blank"
          >
            <Image
              alt={sponsor.name}
              className="rounded-2xl shadow-lg"
              height={100}
              src={sponsor.src}
              width={150}
            />
          </NextLink>
        ))}
      </div>
    </div>
  );
};
