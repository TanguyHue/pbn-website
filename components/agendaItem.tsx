import { agendaType, agenda } from "@/config/agenda";

export const agendaItem = (params: agendaType) => {
  return (
    <div
      key={params.title}
      className="p-4 bg-beige-fonce rounded-lg shadow-md transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
    >
      <h3 className="font-semibold mb-2">
        {`${params.hour} - ${params.title}`}
      </h3>
      <p>{params.label}</p>
    </div>
  );
};

export const AgendaSection = () => {
  return (
    <div className="text-lg md:text-xl space-y-6">
      {agenda.map((item) => agendaItem(item))}
    </div>
  );
};
