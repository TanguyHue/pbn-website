import "@/styles/globals.css";
import { Metadata } from "next";
import clsx from "clsx";
import { SpeedInsights } from "@vercel/speed-insights/next";
import Link from "next/link";

import { Providers } from "./providers";

import { siteConfig } from "@/config/site";
import { fontSans } from "@/config/fonts";
import { Navbar } from "@/components/navbar";
import ScrollToTopButton from "@/components/scrollToTopButton";

export const metadata: Metadata = {
  title: {
    default: siteConfig.name,
    template: `%s - ${siteConfig.name}`,
  },
  description: siteConfig.description,
  icons: {
    icon: "/favicon.ico",
  },
};

export default function RootLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  return (
    <html
      suppressHydrationWarning
      className="scroll-smooth"
      lang="fr"
      style={{ scrollBehavior: "smooth" }}
    >
      <head />
      <body
        className={clsx(
          "min-h-screen bg-beige font-sans antialiased text-black",
          fontSans.variable,
        )}
      >
        <Providers>
          <div className="relative flex flex-col h-screen">
            <Navbar />
            <main className="flex-grow">{children}</main>
            <ScrollToTopButton />
            <footer className="w-full flex  flex-col justify-center py-3 bg-beige">
              <div className="flex items-center justify-center">
                {siteConfig.footerItems.map((item) => (
                  <Link
                    key={item.label}
                    className="text-lg mx-2 hover:text-blue-500 transition duration-400"
                    href={item.href}
                    target="_blank"
                  >
                    {item.label}
                  </Link>
                ))}
              </div>
              <div className="hidden md:block w-full text-center">
                <span className=" text-gray-600">
                  @2024 Copyright -{" "}
                  <span className="font-medium text-gray-800">
                    Polytech By Night BDE Polytech Nantes
                  </span>
                </span>
              </div>
              <div className="block md:hidden w-full text-center">
                <p className="font-medium text-gray-800">
                  Polytech By Night BDE Polytech Nantes
                </p>
                <p className="text-gray-600">@2024 Copyright</p>
              </div>
            </footer>
            <SpeedInsights />
          </div>
        </Providers>
      </body>
    </html>
  );
}
