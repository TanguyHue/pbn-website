import { Button } from "@nextui-org/button";
import Link from "next/link";
import Image from "next/image";

import { siteConfig } from "@/config/site";
import { AgendaSection } from "@/components/agendaItem";
import { SponsorsSection } from "@/components/sponsors";

export default function Home() {
  return (
    <div className="bg-beige">
      {/* Hero Section */}
      <section className="relative flex flex-col items-center justify-center gap-6 py-8 md:py-20 md:h-[calc(100vh-5rem)]">
        <video
          autoPlay
          loop
          muted
          playsInline
          className="absolute inset-0 w-full h-full object-cover"
        >
          <source src="/assets/video/teaser.mp4" type="video/mp4" />
          Your browser does not support the video tag.
        </video>
        <div className="absolute inset-0 bg-black opacity-50" />
        <div className="relative z-10 text-center text-white px-4">
          <h1 className="text-5xl md:text-6xl font-extrabold mb-6 drop-shadow-lg">
            {siteConfig.name}
          </h1>
          <p className="text-xl md:text-2xl mb-8 drop-shadow-lg">
            Venez vivre une soirée magique à notre gala le{" "}
            <strong>16 novembre</strong>
          </p>
          <div className="flex justify-center mt-8">
            <Image
              alt="Gala"
              className="rounded-none"
              height={300}
              src={"/assets/logo_temp_white.png"}
              width={300}
            />
          </div>
          <div className="flex flex-col md:flex-row justify-center items-center mt-10 gap-6">
            <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
              <Button
                as={Link}
                className="font-bold border-3 border-white p-4 text-xl"
                color="primary"
                href="/gala"
                size="lg"
              >
                <span className="mr-2 text-2xl">✨</span> Découvrir notre gala
              </Button>
            </div>
            <div className="transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
              <Button
                as={Link}
                className="font-bold border-3 border-white p-4 text-xl"
                color="secondary"
                href="/billetterie"
                size="lg"
              >
                <span className="mr-2 text-2xl">🎫</span> Prendre votre billet
              </Button>
            </div>
          </div>
        </div>
      </section>
      <div className="container mx-auto max-w-7xl py-6 md:py-12 text-center">
        {/* About Section */}
        <section className="py-12 md:py-20">
          <div className="container mx-auto px-8 md:px-20 text-center">
            <h2 className="text-4xl font-bold mb-6">✨ À propos du Gala</h2>
            <p className="text-lg md:text-xl mb-16 text-justify md:text-center">
              {`Le gala de Polytech Nantes est l'événement le plus attendu de
              l'année. Une soirée inoubliable remplie de musique, de danse, et
              de moments magiques. Rejoignez-nous pour célébrer ensemble et
              créer des souvenirs mémorables.`}
            </p>
            <Image
              alt="À propos du Gala"
              className="rounded-lg shadow-lg mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={400}
              src="/assets/about.jpg"
              width={600}
            />
          </div>
        </section>

        {/* Agenda Section */}
        <section className="py-12 md:py-20">
          <div className="container mx-auto px-4 md:px-20 text-center">
            <h2 className="text-4xl font-bold mb-6">🕒 Horaire de la Soirée</h2>
            <AgendaSection />
          </div>
        </section>

        {/* Sponsors Section */}
        <section className="py-12 md:py-20">
          <div className="container mx-auto px-8 md:px-20 text-center">
            <h2 className="text-4xl font-bold mb-6">🎁 Nos Sponsors</h2>
            <p className="text-lg md:text-xl mb-8 text-justify md:text-center">
              Nous remercions chaleureusement nos sponsors pour leur soutien.
              Leur contribution rend cet événement possible.
            </p>
            <SponsorsSection />
          </div>
        </section>
      </div>
    </div>
  );
}
