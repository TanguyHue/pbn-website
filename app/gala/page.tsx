"use client";

import { Button } from "@nextui-org/button";
import Link from "next/link";
import Image from "next/image";
import clsx from "clsx";

export default function AboutPage() {
  return (
    <div className="bg-beige">
      <section className="relative flex flex-col items-center justify-center gap-6 py-8 md:py-20">
        <div className="container mx-auto px-8 text-center">
          <h1 className="text-5xl md:text-6xl font-extrabold mb-6 drop-shadow-xl">
            Polytech By Night 2024
          </h1>
          <h2 className="text-3xl md:text-4xl mb-8">
            Redonnons à Polytech By Night tout son éclat !
          </h2>
          <div className="flex justify-center mt-8">
            <Image
              alt="Polytech By Night"
              className="shadow-xl rounded-lg mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={300}
              src="/assets/gala-about.jpg"
              width={900}
            />
          </div>
        </div>
      </section>

      <div className="container mx-auto max-w-7xl">
        <section className="py-12 md:py-20">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">
              {"🎉 Présentation de l'Édition"}
            </h2>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`L'édition 2024/2025 revêt une importance particulière. Notre objectif est clair : redonner à Polytech By Night tout son éclat, en mettant en avant les valeurs fondamentales qui ont toujours fait de cet événement un moment inoubliable.`}
            </p>

            <h3 className="text-2xl md:text-3xl mt-8 text-primary font-bold text-center mb-16">
              Ce 16 novembre, venez découvrir un gala exceptionnel !
            </h3>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`Cette année, nous adoptons une approche plus modeste, conscient que la grandeur de l'événement ne réside pas uniquement dans son ampleur, mais surtout dans l'atmosphère chaleureuse et conviviale qu'il offre. Nous souhaitons recréer l'esprit de camaraderie et de célébration qui a toujours caractérisé Polytech By Night.`}
            </p>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`Nous nous concentrons sur l'essentiel : créer un environnement où chacun se sent chez lui, où les liens se tissent et se renforcent, où les souvenirs se créent et perdurent. Malgré les difficultés rencontrées lors de la précédente édition, nous sommes plus motivés que jamais à faire de cette édition un succès retentissant.`}
            </p>
            <Image
              alt="Présentation de l'Édition"
              className="rounded-lg shadow-xl mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={400}
              src="/assets/presentation.JPG"
              width={600}
            />
          </div>
        </section>

        {/* Hidden Theme Section */}
        <section className="py-12 md:py-20 relative">
          <div className="container mx-auto px-8 md:px-20 text-center">
            <h2 className="text-4xl font-bold mb-6">
              {"🎨 Thème de l'Édition"}
            </h2>
            <div className="relative">
              <div
                className={clsx(
                  "relative z-10 text-lg md:text-xl mt-16 p-6 rounded-lg shadow-xl bg-beige-fonce",
                )}
                id="theme-reveal"
              >
                <p className="mb-2">
                  Pour cette édition, nous avons décidé de choisir comme thème
                </p>
                <span className="fond-extrabold text-2xl md:text-3xl mt-2">
                  {`🚀 L'Odysée Temporelle ! 🎉`}
                </span>
              </div>
            </div>
          </div>
        </section>

        {/* Stages and Artists Section */}
        <section className="py-12 md:py-20">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">
              🎤 Scènes et Artistes
            </h2>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`Nous avons élaboré un plan de mise en scène ambitieux, avec la création de deux scènes de tailles différentes. Cette diversification permettra de proposer une programmation variée tout en préservant la convivialité et la proximité caractéristiques de notre événement.`}
            </p>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`Comme dans les éditions précédentes, nous souhaitons également mettre en avant les talents locaux. Les participants auront l'opportunité de découvrir une multitude d'artistes nantais, qu'ils proviennent de Polytech, d'autres écoles nantaises ou même du milieu professionnel.`}
            </p>
            <Image
              alt="Scènes et Artistes"
              className="rounded-lg shadow-xl mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={400}
              src="/assets/stages_artists.jpg"
              width={600}
            />
          </div>
        </section>

        {/* Goals and Vision Section */}
        <section className="py-12 md:py-2">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">
              🌟 Objectifs et Vision
            </h2>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`En somme, notre ambition pour cette édition de Polytech By Night est de créer une expérience mémorable, où la convivialité et la chaleur humaine sont au cœur de chaque instant. Nous sommes déterminés à offrir un événement à la hauteur des attentes de notre communauté, tout en restant fidèles à nos valeurs et à notre vision d'un gala inoubliable.`}
            </p>
            <p className="text-lg md:text-xl mb-16 text-justify">
              {`Avec cette édition, nous aspirons également à insuffler un nouvel élan, redonnant ainsi un souffle nouveau à notre cher Polytech By Night, pour qu'il continue à briller comme l'un des moments forts de la vie étudiante à Nantes.`}
            </p>
            <Image
              alt="Objectifs et Vision"
              className="rounded-lg shadow-xl mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={400}
              src="/assets/goals_vision.jpg"
              width={600}
            />
          </div>
        </section>

        {/* Call to Action Section */}
        <section className="py-12 md:py-20 px-8">
          <div className="container mx-auto px-4 md:px-8 text-center bg-beige-fonce rounded-lg shadow-xl py-8">
            <h2 className="text-4xl font-bold mb-6">🎊 Rejoignez-nous</h2>
            <p className="text-lg md:text-xl mb-16">
              {`Nous sommes impatients de partager cette aventure avec vous et de faire de Polytech By Night 2024/2025 un événement mémorable. Prenez votre billet et rejoignez-nous pour une soirée inoubliable !`}
            </p>
            <div className="flex justify-center mt-8 transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
              <Button
                as={Link}
                className="font-bold p-4 text-xl bg-primary text-white rounded-lg shadow-xl rounded-lg"
                href="/billetterie"
                size="lg"
              >
                <span className="mr-2 text-2xl">🎫</span> Prendre votre billet
              </Button>
            </div>
          </div>
        </section>
      </div>
    </div>
  );
}
