"use client";

import Image from "next/image";
import { useState } from "react";
import { Button } from "@nextui-org/button";

import { AgendaSection } from "@/components/agendaItem";

export default function AboutPage() {
  const [mapOpen, setMapOpen] = useState(false);

  return (
    <div className="bg-beige">
      <section className="relative flex flex-col items-center justify-center gap-6 py-8 md:py-20">
        <div className="container mx-auto px-8 text-center">
          <h1 className="text-5xl md:text-6xl font-extrabold mb-6 drop-shadow-lg">
            Polytech By Night 2024
          </h1>
          <h2 className="text-3xl md:text-4xl mb-8">Infos pratiques</h2>
          <div className="flex justify-center mt-8">
            <Image
              alt="Polytech By Night"
              className="shadow-xl rounded-lg mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={300}
              src="/assets/infos-pratiques.jpg"
              width={900}
            />
          </div>
        </div>
      </section>

      <div className="container mx-auto max-w-7xl py-12 md:py-20 text-center">
        {/* Schedule Section */}
        <section className="py-1">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">
              {"🕒 Horaire de l'Événement"}
            </h2>
            <AgendaSection />
          </div>
        </section>

        {/* Access Section */}
        <section className="py-12">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">🚗 Accès</h2>
            <p className="text-lg md:text-xl mb-8 text-justify">
              Vous pouvez accéder au Gala par voiture (un parking est disponible
              sur le site de Polytech) mais le plus simple reste le bus C6. Le
              terminus « Chantrerie Grandes Écoles » de cette ligne arrive juste
              en face de l’école Polytech Nantes.
            </p>
            <p className="text-lg md:text-xl mb-8 text-justify md:text-center">
              L’adresse exacte est « Rue Christian Pauc, 44300 Nantes ».
            </p>
            <div className="flex flex-col md:flex-row justify-center text-center transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1">
              <Button
                className="rounded-xl shadow-xl"
                color="primary"
                size="lg"
                onClick={() => setMapOpen(!mapOpen)}
              >
                <span className="font-bold">
                  {mapOpen ? "Fermer la carte" : "Afficher la carte"}
                </span>
              </Button>
            </div>
            {mapOpen && (
              <div className="mt-8">
                <iframe
                  allowFullScreen
                  aria-hidden="false"
                  className="w-full h-96 rounded-lg shadow-lg"
                  src="https://www.google.com/maps/d/u/0/embed?mid=1ZflV2wknncvdDjFLu8j-pQIR-abcMN8&ehbc=2E312F&noprof=1"
                  title="Polytech Nantes Location"
                />
              </div>
            )}
            <div className="flex flex-col md:flex-row justify-center mt-6 gap-4">
              <div className="flex flex-col transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1 rounded-xl shadow-xl mt-4 md:mt-0">
                <Button
                  as="a"
                  color="success"
                  href="https://www.google.com/maps/dir//Polytech+Nantes,+Rue+Christian+Pauc,+44300+Nantes"
                  rel="noopener noreferrer"
                  size="lg"
                  target="_blank"
                >
                  <span className="font-bold text-white">
                    Ouvrir dans Google Maps
                  </span>
                </Button>
              </div>
              <div className="flex flex-col transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1 rounded-xl shadow-xl">
                <Button
                  as="a"
                  color="success"
                  href="https://waze.com/ul?ll=47.281568,-1.515691&navigate=yes"
                  rel="noopener noreferrer"
                  size="lg"
                  target="_blank"
                >
                  <span className="font-bold text-white">Ouvrir dans Waze</span>
                </Button>
              </div>
            </div>
          </div>
        </section>

        {/* Transportation Section */}
        <section className="py-12">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">
              🚌 Comment s’y rendre ?
            </h2>
            <p className="text-lg md:text-xl mb-8 text-justify md:text-center">
              L’événement est accessible en voiture ou en bus.
            </p>
            <p className="text-lg md:text-xl mb-8 text-justify">
              Pour la voiture, il vous suffit d’emprunter le périphérique
              Nantes-Est et de prendre la sortie « 24-Nantes-Est ». Un parking
              est disponible juste devant l’école. Attention toutefois car sa
              capacité est limitée.
            </p>
            <p className="text-lg md:text-xl mb-8 text-justify">
              Le plus simple reste donc d’emprunter la ligne de bus C6. Celle-ci
              a pour terminus « Chantrerie Grandes Écoles », juste en face de
              l’école Polytech Nantes. Pour le retour, le dernier bus part de
              l’école à 02h18.
            </p>
          </div>
        </section>

        {/* Payment Section */}
        <section className="py-12">
          <div className="container mx-auto px-8 md:px-20">
            <h2 className="text-4xl font-bold mb-6 text-center">
              💳 Comment payer le jour J ?
            </h2>
            <p className="text-lg md:text-xl mb-8 text-justify">
              Pour ce faire, rien de plus simple, tout se passe sur
              l’application « Lyf Pay » (disponible sur iOS et Android). Lyf Pay
              vous permet de faire des transactions d’argent d’un téléphone à un
              autre. Pour cela, il vous suffit d’entrer votre carte bancaire sur
              l’application et lors du paiement vous n’aurez qu’un code-barres à
              présenter à votre interlocuteur.
            </p>
            <div className="flex flex-col md:flex-row justify-center mb-8 gap-4">
              <div className="flex flex-col transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1 rounded-xl shadow-xl mb-4 md:mb-0">
                <Button
                  as="a"
                  color="success"
                  href="https://play.google.com/store/apps/details?id=com.fivory.prod&hl=fr"
                  rel="noopener noreferrer"
                  size="lg"
                  target="_blank"
                >
                  <span className="font-bold text-white">
                    Télécharger sur Android
                  </span>
                </Button>
              </div>
              <div className="flex flex-col transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1 rounded-xl shadow-xl mb-4 md:mb-0">
                <Button
                  as="a"
                  color="success"
                  href="https://apps.apple.com/fr/app/lyf-pay/id824967438"
                  rel="noopener noreferrer"
                  size="lg"
                  target="_blank"
                >
                  <span className="font-bold text-white">
                    Télécharger sur iOS
                  </span>
                </Button>
              </div>
            </div>

            <p className="text-lg md:text-xl mb-8">
              Il est important de noter que l’ensemble des bars arrête le
              service à 2h.
            </p>
          </div>
        </section>
      </div>
    </div>
  );
}
