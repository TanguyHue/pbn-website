"use client";
import Image from "next/image";

import { Contact } from "@/components/contact";
import { Tarifs } from "@/components/tarif";
import { BilletterieAccess } from "@/components/billetterieAccess";

export default function TicketingPage() {
  return (
    <div className="bg-beige text-center">
      <section className="relative flex flex-col items-center justify-center gap-6 py-8 md:py-20">
        <div className="container mx-auto px-8 text-center">
          <h1 className="text-5xl md:text-6xl font-extrabold mb-6 drop-shadow-xl">
            Polytech By Night 2024
          </h1>
          <h2 className="text-3xl md:text-4xl mb-8">
            Bienvenue sur la Billetterie
          </h2>
          <div className="flex justify-center mt-8">
            <Image
              alt="Polytech By Night"
              className="shadow-xl rounded-lg mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={300}
              src="/assets/billetterie.jpg"
              width={900}
            />
          </div>
        </div>
      </section>

      {/* Pricing Section */}
      <Tarifs />

      {/* HelloAsso Section */}
      <section className="py-12">
        <div className="container mx-auto px-8 text-center">
          <h2 className="text-4xl font-bold mb-6">📅 Réservez vos billets</h2>
          <p className="text-md md:text-lg mb-2 text-center">
            {`Pour les billets Repas (réservés aux diplomés, on rappelle !), vous avez reçu un lien par mail pour avoir la billetterie.`}
          </p>
          <p className="text-md md:text-lg mb-8 text-center">
            {`Si ce n'est pas le cas, contactez-nous sur Instagram ou par mail.`}
          </p>
          <BilletterieAccess />
        </div>
      </section>

      {/* Contact Section */}
      <Contact />
    </div>
  );
}
