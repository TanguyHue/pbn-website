"use client";

import Image from "next/image";

import { EditionsPrecedentes } from "@/components/editions-precedentes";
import { Contact } from "@/components/contact";

export default function AboutPage() {
  return (
    <div className="bg-beige">
      <section className="relative flex flex-col items-center justify-center gap-6 py-8 md:py-20">
        <div className="container mx-auto px-8 text-center">
          <h1 className="text-5xl md:text-6xl font-extrabold mb-6 drop-shadow-lg">
            Polytech By Night 2024
          </h1>
          <h2 className="text-3xl md:text-4xl mb-8 drop-shadow-lg">
            Qui sommes-nous ?
          </h2>
          <div className="flex justify-center mt-8">
            <Image
              alt="Polytech By Night"
              className="shadow-xl rounded-lg mx-auto transition duration-500 ease-in-out transform hover:scale-105 hover:-translate-y-1"
              height={300}
              src="/assets/qui-sommes-nous.jpg"
              width={900}
            />
          </div>
        </div>
      </section>

      <div className="container mx-auto max-w-7xl py-12 md:py-20 text-center">
        {/* The Club Section */}
        <section className="py-12">
          <div className="container mx-auto px-8">
            <h2 className="text-4xl font-bold mb-6 text-center">
              👥 Le Club PBN
            </h2>
            <p className="text-lg md:text-xl mb-8 text-justify">
              {`Le club Polytech By Night (PBN) est une équipe dynamique chargée
              d'organiser chaque année le gala de Polytech Nantes. Nos membres
              sont généralement en dernière année et proviennent de toutes les
              filières : Matériaux, ETN, Informatique, et TEM. 💼 `}
            </p>
            <p className="text-lg md:text-xl mb-8 text-justify">
              {`Notre mission ? Vous offrir une soirée exceptionnelle ! Nous nous
              investissons pleinement pour préparer un événement de qualité,
              avec des artistes variés, un menu délicieux, et une décoration qui
              rivalise avec celle d'Hollywood. 🎤🍽️✨`}
            </p>
            <p className="text-lg md:text-xl mb-8 text-justify">
              Venez avec votre bonne humeur et votre plus grand sourire ! 😄 Et
              si vous êtes à Polytech Nantes, peu importe votre filière ou
              niveau, rejoignez-nous dans cette aventure incroyable. Nous avons
              besoin de vous ! 🚀
            </p>
          </div>
        </section>

        {/* Previous Editions Section */}
        <section className="py-12">
          <div className="container mx-auto px-8">
            <h2 className="text-4xl font-bold mb-6 text-center">
              🎉 Éditions Précédentes
            </h2>
            <p className="text-lg md:text-xl mb-8 text-justify md:text-center">
              Découvrez les moments forts des éditions passées de Polytech By
              Night. Revivez les souvenirs, les moments mémorables, et la magie
              de nos événements précédents !
            </p>
            <EditionsPrecedentes />
          </div>
        </section>

        {/* Contact Section */}
        <Contact />
      </div>
    </div>
  );
}
